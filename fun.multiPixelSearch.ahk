﻿;*********************************************************************************************************************************************************************
;
; multiImageSearch()
;
;********************************************************************************************************************************************************************
;
; author: kapitalny (Maciek Caderek)
; last edit: 2015-03-28 06:45
; Licensed under CC BY-SA 3.0 -> http://creativecommons.org/licenses/by-sa/3.0/
;
; Requires:
; Gdip by tic (Tariq Porter) - http://www.autohotkey.com/board/topic/29449-gdi-standard-library-145-by-tic/
; Gdip_ImageSearch() by MasterFocus - https://github.com/MasterFocus/AutoHotkey/blob/master/Functions/Gdip_ImageSearch/Gdip_ImageSearch.ahk
;
;********************************************************************************************************************************************************************
;
;--------------------------------
; ++OPIS++
;--------------------------------
;
; Funkcja posiada dwa główne mechanizmy:
; 1) Wielokrotnie wyszukuje piksel danego koloru - przydatne gdy szukany element nie jest widoczny na ekranie cały czas.
; 2) Wyszukuje piksel wśród całego zestawu kolorów - przydatne gdy w danym punkcie mogą znajdować się elementy o różnych kolorach
;    (odpowiednik pixelGetColor z tą różnicą, że szuka koloru wśród kolorów zadeklarowanych przez użytkownika a nie wśród wszystkich możliwych)
; Oba powyższe machanizmy można zastosować równocześnie.
;
;--------------------------------
; ++PARAMETRY++
;--------------------------------
;
;-> colorSet [string]:             (wymagana) zmienna określa ścieżkę dostępu do wzorca lub zestawu wzorców (aby podac zestaw wzorcow należy rozdzielic nazwy plikow znakami |)
;                                 uwaga: jeżeli określono zestaw wzorców to funkcja sprawdza je po kolei i kończy działanie jeśli znajdzie jeden z nich 
;                                        (kolejnosc sprawdzania zgodna z zapisem)
;
;       PRZYKŁADY
;       "0xFFFFFF"
;       "0x000000|0xFF00FF|0x00FFFF"
;
;-> coordSet [string]:            (opcjonalna) zmienna zawiera ciąg ustawień współrzędnych wejściowych i wyjściowych
;
;       Domyślna wartość:         "0,0 | 0 | 0,0"
;       Schemat ciągu:            "inputCoords | tolerance | outputCoords"
;       Objaśnienie:              inputCoords      - koordynanty obszaru wyszukiwania - dozwolone jest podanie dwóch lub czterech współrzędnych:
;                                                       a) dwie współrzędne - użytkownik określa spodziewany punkt początkowy wystąpienia wzorca na ekranie,
;                                                       funkcja automatycznie ustawi jako obszar szukania dany punkt powiększony z każdej strony o wartość parametru tolerance
;                                                       b) cztery współrzedne - użytkownik określa dokładny obszar szukania - punkt początkowy i końcowy   
;                                 tolerance        - rozszerzenie obszaru wyszukiwania w stosunku do wielkości wzorca (ma zastosowanie gdy użytkownik poda dwie współrzędne w parametrze inputCords
;                                 outputCoords     - względne wobec wzorca położenie współrzędnych zwracanych przez funkcję, dozwolone wartości:
;                                                    dowolny punkt względem lewego górnego rogu wzorca  - jeśli stosujemy zestaw wzorców możemy podać odpowiadajaca mu liczbę
;                                                    par współrzędnych lub jedną wspólną parę współrzędnych, współrzędne oddzielamy znakami "," a poszczególne pary znakami ";"
;       
;       PRZYKŁADY
;       "50, 60"
;       "0, 0, 800, 600 | 10, 15"
;       "37,200 | 10,15 ; 13,10 ; 14,27"
;       "| ; 25,20 ; 30,50 ; ; 20,25"               uwaga: w przypadku outputCords pominięte elementy zostaną zastąpione wartością pierwszego elementu,
;                                                          jeśli pierwszy element zostanie pominięty przyjmie on wartość domyślną (topLeft)
;
;-> searchSet [string]:           (opcjonalna) zmienna zawiera ciąg standardowych ustawień wyszukiwania dla funkcji PixelSearch
;                                 uwaga: poniżej zamieszczono tylko skrótowe informacje - pełen opis: http://ahkscript.org/docs/commands/PixelSearch.htm
;
;       Domyślna wartość:         "0 | fast"
;       Schemat ciągu:            "variation | instances | trans | searchDirection | lineDelim | coordDelim"
;       Objaśnienie:              variation        - liczba dopuszczalnych wariacji (odcieni) każdej z wartości RGB - dozwolone wartości: 0-255
;                                 mode             - tryb wyszukiwania - dozwolone wartości: fast, slow, RGB
;       
;       PRZYKŁADY
;       "40 | 0 "                   uwaga: pominięte parametry przyjmą ustawienia domyślne
;       "20 |   | 0xFFFFFF | 8"     uwaga: wszelkie spacje są opcjonalne
;
;-> repeatSet [string]:           (opcjonalna) zmienna zawiera ciąg ustawień powtarzania wyszukiwania wzorca
;
;       Domyślna wartość:         "1 | 500"
;       Schemat ciągu:            "repeatMax | repeatPeriod"
;       Objaśnienie:              repeatMax        - maksymalna liczba powtórzeń wyszukiwania - dozwolone wartości: 1+
;                                 repeatPeriod     - odstęp w milisekundach pomiędzy kolejnymi próbami wyszukania wzorca - dozwolone wartości: 0+
;       
;       PRZYKŁADY
;       "5"                       uwaga: pominięte parametry przyjmą ustawienia domyślne
;       "20 | 1000"               uwaga: wszelkie spacje są opcjonalne
;
;-> clientSet [string]:           (opcjonalna) zmienna zawiera ciąg ustawień okna roboczego
;
;       Domyślna wartość:         "BlueStacks App Player | 868 | 720"
;       Schemat ciągu:            "winName | clientW | clientH"
;       Objaśnienie:              winName          - nazwa używanego okna
;                                 clientW          - szerokość obszaru roboczego używanego okna
;                                 clientH          - wysokość obszaru roboczego używanego okna
;       
;       PRZYKŁADY
;       "Window Name | 800 | 600"      
;
;
;--------------------------------
; ++ZWRACANE WARTOŚCI++
;--------------------------------
;
;   Funkcja zwraca tablicę asocjacyjną zawierającą następujące elementy:
;       
;       arrayName["hits"]         - liczba trafień: 1 - znaleziono wzorzec, 0 - nie znaleziono wzorca, liczba ujemna - błąd podczas wykonywania funkcji
;       arrayName["coord"]*       - współrzędne punktu, w którym znaleziono piksel danego koloru (lub współrzędne zmodyfikowane przez parametr outputCoords)
;       arrayName["color"]*       - znaleziony kolor
;
;       *elementy oznaczone gwiazdką będą dostępne tylko jeśli funkcja zaliczy trafienie
;
;********************************************************************************************************************************************************************
;
;   UWAGA: Jeśli pozostawisz wszstkie wartości jako domyślne wynik działania funkcji będzie podobny do zwykłego PixelSearch
;
;********************************************************************************************************************************************************************
;********************************************************************************************************************************************************************

;#Include libs\Gdip.ahk
;#Include libs\Gdip_ImageSearch.ahk

multiPixelSearch(colorSet, coordSet = "", searchSet = "", repeatSet = "", clientSet = "")
{
    result := [] ; zainicjowanie tablicy wyników
    
    ;##### USTAWIENIA DOMYŚLNE #####
    
    variation := 0
    instances := 1
    mode := "fast"
    lineDelim := "+"
    coordDelim := ","
    
    repeatMax := 1
    repeatPeriod := 500
    
    winName := "BlueStacks App Player"
    clientW := 868
    clientH := 720
    
    inputCoords := [0, 0]
    tolerance := 0
    outputCords := "0,0"
    
    ;##### WCZYTYWANIE USTAWIEŃ #####
    
    pixColors := StrSplit(colorSet, "|", " ")
    
    if(searchSet != "")
    {
        searchSet := StrSplit(searchSet, "|", " ")
        searchSetCount := searchSet._MaxIndex()
        if(searchSet[1] != "")
            variation := searchSet[1]
        if(searchSetCount > 1 AND searchSet[2] != "")
            mode := searchSet[2]
    }
    
    if(repeatSet != "")
    {
        repeatSet := StrSplit(repeatSet, "|", " ")
        repeatSetCount := repeatSet._MaxIndex()
        if(repeatSet[1] != "")
            repeatMax := repeatSet[1]
        if(repeatSetCount > 1 AND repeatSet[2] != "")
            repeatPeriod := repeatSet[2]
    }
    
    if(clientSet != "")
    {
        clientSet := StrSplit(clientSet, "|", " ")
        clientSetCount := clientSet._MaxIndex()
        if(clientSet[1] != "")
            winNeme := clientSet[1]
        if(clientSetCount > 1 AND clientSet[2] != "")
            clientW := clientSet[2]
        if(clientSetCount > 2 AND clientSet[3] != "")
            clientH := clientSet[3]
    }
    
    if(coordSet != "")
    {
        coordSet := StrSplit(coordSet, "|", " ")
        coordSetCount := coordSet._MaxIndex()
        if(coordSet[1] != "")
        {
            inputCoords := coordSet[1]
            inputCoords := StrSplit(inputCoords, ",", " ")
        }
        if(coordSetCount > 1 AND coordSet[2] != "")
        {
            tolerance := coordSet[2]
        }
        if(coordSetCount > 2 AND coordSet[3] != "")
        {
            outputCords := coordSet[3]
        }
    }
    outputCords := StrSplit(outputCords, ";", " ")
    
    if(inputCoords._MaxIndex() = 4)
    {
        x1 := inputCoords[1]
        y1 := inputCoords[2]
        x2 := inputCoords[3]
        y2 := inputCoords[4]
    }
    else if(inputCoords._MaxIndex() = 2)
    {
        x1 := inputCoords[1] - tolerance
        y1 := inputCoords[2] - tolerance
        x2 := inputCoords[1] + tolerance
        y2 := inputCoords[2] + tolerance
    }
    else
        MsgBox Podano nieprawidłąwą wartość parametru inputCoords - podaj dwie lub cztery współrzędne.
        
    ;##### WYSZUKIWANIE PIKSELI #####
    
    i := 0
    Loop ; pętla powtarza wyszukiwanie zadana ilość razy (lub przerywa gdy trafi)
    {
        i++
        if(i > repeatMax)
        {
            result["hits"] := 0
            return result
        }
        else if(i > 1)
            Sleep repeatPeriod
        ;guiBar(i)
            
        for index, pixColor in pixColors
        {  
            ;MsgBox Sprawdzam kolor: x1: %x1% | y1: %y1% | x2: %x2% | y2: %y2% | pixColor: %pixColor% | variation: %variation% | mode: %mode%
            ;##################################################################
            PixelSearch, outX, outY, %x1%, %y1%, %x2%, %y2%, %pixColor%, %variation%, %mode%
            ;##################################################################
            if !ErrorLevel
            {
                if(outputCords[index] AND outputCords[index] != "")
                    outShift := outputCords[index]
                else if(outputCords[1] != "")
                    outShift := outputCords[1]  
                else
                    outShift := "0,0"  
                ;MsgBox % outShift

                outShift := StrSplit(outShift, ",", " ")
                xShift := outShift[1]
                yShift := outShift[2]
                                     

                x := Round(outX + xShift)
                y := Round(outY + yShift)

                
                result["hits"] := 1
                result["coords"] := x "," y
                result["color"] := pixColor
                ;MsgBox % result["coords"]
                return result
            }
                
            if(index = colors._MaxIndex())
                break
        }
    }
}